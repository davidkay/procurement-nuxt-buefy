import feathers from '@/plugins/feathers'

export const state = () => ({
  submissions: null
})

export const mutations = {
  SET_SUBMISSIONS(state, payload) {
    // console.info('vuex payload: ', payload)
    state.submissions = payload
  }
}

export const getters = {
  isAuthenticated(state) {
    return !!state.user
  },
  submissions(state) {
    return state.submissions
  }
}

export const actions = {
  getSubmissions({ commit }) {
    return feathers.service('procurement-submissions').find()
      .then((response) => {
        console.log('procsubmit response: ', response)
        commit('SET_SUBMISSIONS', response.data)
      })
  }
}
